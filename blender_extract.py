#!/usr/bin/env python3

import os
import re
import sys
import mmap
import argparse
import contextlib


PRG_VERSION = '2.0'
PRG_NAME = os.path.basename(sys.argv[0])


def main():

    # command line

    parser = argparse.ArgumentParser(
        description='{} V{}'.format(PRG_NAME, PRG_VERSION))

    parser.add_argument(
        'file_exe',
        action='store',
        metavar='file.exe',
        help='Input EXE file')

    parser.add_argument(
        'file_blend',
        action='store',
        nargs='?',
        metavar='file.blend',
        help='Output Blender file')

    # parse command line
    args = parser.parse_args()

    file_exe = args.file_exe
    file_blend = args.file_blend

    if file_blend is None:
        (name, _) = os.path.splitext(file_exe)
        file_blend = '{}.blend'.format(name)

    if os.path.exists(file_blend):
        print('Error: file "%s" exists' % file_blend, file=sys.stderr)
        sys.exit(1)

    # open input file
    with open(file_exe, 'r+b') as file_in:

        # memory mapped input file
        with contextlib.closing(mmap.mmap(file_in.fileno(), 0, access=mmap.ACCESS_READ)) as data:

            # regular expression
            pattern = re.compile(b'BLENDER[_-][vV][0-9]{3}')

            # search blender's header
            result = re.search(pattern, data)

            if (not result):
                print('No BLENDER header found', file=sys.stderr)
                sys.exit(1)

            # offset file
            offset = result.start()

            # open output file
            with open(file_blend, 'wb') as file_out:
                # write data
                file_out.write(data[offset:])

            if (data[offset + 7] == '_'):
                bits = 32
            else:
                bits = 64

            if (data[offset + 8] == 'v'):
                endian = 'Little'
            else:
                endian = 'Big'

            version = '{}.{}{}'.format(
                chr(data[offset + 9]), chr(data[offset + 10]), chr(data[offset + 11]))

    # report

    print('File input: "{}"'.format(file_exe))
    print('Offset: {} {:#x}'.format(offset, offset))
    print('File output: "{}"'.format(file_blend))
    print('{} bits'.format(bits))
    print('{} endian'.format(endian))
    print('Version: {}'.format(version))


if __name__ == '__main__':
    main()
